<?php

return [
  'name'               => 'Name',
  'value'              => 'Value',
  'description'        => 'Description',
  'setting_singular'   => 'setting',
  'setting_plural'     => 'settings',

];
