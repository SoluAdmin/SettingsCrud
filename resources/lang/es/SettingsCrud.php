<?php

return [
  'name'               => 'Nombre',
  'value'              => 'Valor',
  'description'        => 'Descripción',
  'setting_singular'   => 'Ajuste',
  'setting_plural'     => 'Ajustes',

];
