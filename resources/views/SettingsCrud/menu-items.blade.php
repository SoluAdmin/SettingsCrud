@module("SoluAdmin\\SettingsCrud")
<li>
    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.SettingsCrud.route_prefix', '') . '/setting') }}">
        <i class="fa fa-cog"></i> <span>{{trans('SoluAdmin::SettingsCrud.setting_plural')}}</span>
    </a>
</li>
@endmodule