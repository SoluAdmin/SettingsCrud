<?php

namespace SoluAdmin\SettingsCrud\Http\Controllers;

use SoluAdmin\SettingsCrud\Http\Requests\SettingRequest as UpdateRequest;
use SoluAdmin\SettingsCrud\Models\Setting;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class SettingCrudController extends BaseCrudController
{
    public function setUp()
    {
        parent::setup();
        $this->crud->denyAccess(['create', 'store', 'delete']);

        $this->crud->enableDetailsRow();
    }

    public function showDetailsRow($id)
    {
        $setting = Setting::where('id', $id)->first();
        return view('SoluAdmin::SettingsCrud.detail_row', ['description' => $setting->description]);
    }

    public function index()
    {
        return parent::index();
    }

    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');

        $this->data['entry'] = $this->crud->getEntry($id);
        $field = (array)json_decode($this->data['entry']->field);
        $field['label'] = trans("SoluAdmin::SettingsCrud.{$field['label']}");

        $this->crud->addField($field);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit') . ' ' . $this->crud->entity_name;

        $this->data['id'] = $id;

        return view($this->crud->getEditView(), $this->data);
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
