<?php

namespace SoluAdmin\SettingsCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class SettingCrudDataTable implements DataTable
{
    public function columns()
    {

        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::SettingsCrud.name'),
            ],
            [
                'name' => 'value',
                'label' => trans('SoluAdmin::SettingsCrud.value'),
            ],
        ];
    }
}
