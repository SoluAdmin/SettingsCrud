<?php

namespace SoluAdmin\SettingsCrud\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class SettingCrudForm implements Form
{
    public function fields()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::SettingsCrud.name'),
                'type' => 'text',
                'attributes' => [
                    'disabled' => 'disabled',
                ],
            ],
            [
                'name' => 'description',
                'label' => trans('SoluAdmin::SettingsCrud.description'),
                'type' => 'text',
                'attributes' => [
                    'disabled' => 'disabled',
                ],
            ],
        ];
    }
}
