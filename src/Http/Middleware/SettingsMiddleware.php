<?php

namespace SoluAdmin\SettingsCrud\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use SoluAdmin\SettingsCrud\Models\Setting;

class SettingsMiddleware
{
    public function handle($request, Closure $next)
    {
        if (!App::runningInConsole() && count(Schema::getColumnListing('settings'))) {
            $settings = Setting::all();

            foreach ($settings as $key => $setting) {
                Config::set('settings.' . $setting->key, $setting->value);
            }
        }


        return $next($request);
    }
}
