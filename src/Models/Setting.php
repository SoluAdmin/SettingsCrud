<?php

namespace SoluAdmin\SettingsCrud\Models;

use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $fillable = ['value'];
    protected $translatable = ['value', 'description', 'name'];
}
