<?php

namespace SoluAdmin\SettingsCrud\Providers;

use SoluAdmin\Support\Providers\CrudServiceProvider;
use SoluAdmin\Support\Helpers\PublishableAssets as Assets;

class SettingsCrudServiceProvider extends CrudServiceProvider
{
    protected $assets = [
        Assets::CONFIGS,
        Assets::SEEDS,
        Assets::TRANSLATIONS,
        Assets::VIEWS,
        Assets::MIGRATIONS,
        Assets::MODULE_MIGRATIONS,
    ];

    protected $resources = [
        'setting' => "SettingCrudController"
    ];
}
